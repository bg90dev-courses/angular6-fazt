<div align="center">

# Angular6 Fazt

[![Project](https://img.shields.io/badge/Project-Course-yellow.svg)][repo-link]
[![Repository](https://img.shields.io/badge/gitlab-purple?logo=gitlab)][repo-link]
[![Language](https://img.shields.io/badge/Angular-D41630?logo=angular)][angular6-link]

Curso de Angular 6 por FaztWeb 

https://www.youtube.com/watch?v=AR1tLGQ7COs

https://www.youtube.com/watch?v=OrCdt865WOg
</div>

## Built with

### Technologies

[<img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/javascript.jpeg" width=50 alt="JavaScript">][javascript-link]
[<img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/angularJS.png" width=50 alt="Angular6">][angular6-link]

### Platforms
[<img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/vscode.png" width=50 alt="VSCode">][vscode-link]

## Authors

<div align="center">

### **Borja Gete**

[![Mail](https://img.shields.io/badge/borjag90dev@gmail.com-DDDDDD?style=for-the-badge&logo=gmail)][borjag90dev-gmail]
[![Gitlab](https://img.shields.io/badge/BorjaG90-purple.svg?&style=for-the-badge&logo=gitlab)][borjag90dev-gitlab]
[![Github](https://img.shields.io/badge/BorjaG90-000000.svg?&style=for-the-badge&logo=github&logoColor=white)][borjag90dev-github]
[![LinkedIn](https://img.shields.io/badge/borjag90-0077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)][borjag90dev-linkedin]

</div>

[borjag90dev-gmail]: mailto:borjag90dev@gmail.com
[borjag90dev-github]: https://github.com/BorjaG90
[borjag90dev-gitlab]: https://gitlab.com/BorjaG90
[borjag90dev-linkedin]: https://www.linkedin.com/in/borjag90/
[repo-link]: https://gitlab.com/bg90dev-courses/angular6-fazt
[vscode-link]: https://code.visualstudio.com/
[javascript-link]: https://www.javascript.com/
[angular6-link]: https://v6.angular.io/docs
