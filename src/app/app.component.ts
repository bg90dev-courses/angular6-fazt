import { Component } from "@angular/core";

import { DataService } from "./data.service";

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.css"],
})
export class AppComponent {
  name: string = 'John Carter';
  age:number = 28;

	users: string[] = ["ryan", "joe", "peter"];
  
  posts = [];

  constructor(private dataService: DataService) {
    this.dataService.getData().subscribe(data => {
      this.posts = data;
      
    })
  }

	sayHello() {
		alert("Hello");
	}
	deleteUser(user) {
		this.users = this.users.filter((userItem) => userItem !== user);
	}

	addUser(newUser) {
		this.users.push(newUser.value);
		newUser.value = "";
    newUser.focus();
		return false; //event.preventDefault
	}

}
